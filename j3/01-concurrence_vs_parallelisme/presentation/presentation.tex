  \documentclass[utf8x, 10pt]{beamer}
\usepackage{amsmath, amsfonts, amssymb}
\usepackage{mdwtab}
%%\usepackage[utf8]{inputenc}
%%\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{mathrsfs}
\usepackage{lmodern}
\usepackage{fourier}

\usetheme{Warsaw}
%\usecolortheme{seagull}
\usecolortheme{crane}
%\includeonlyframes{current}
 \setbeamercovered{transparent}

\newcommand{\vect}[1]{\kern 2pt\overrightarrow{\kern -2pt #1 \kern 2pt}}

\begin{document}

\title{Programmation concurrente}
\author{Pascal Grossé}
\date{Jeudi 12 novembre 2020}

\begin{frame}[t]
  \titlepage
\end{frame}

\begin{frame}[t]
  \begin{block}{Plan de la présentation}

    \vspace{3mm} 
    \tableofcontents
    \vspace{3mm}
    
  \end{block}
\end{frame} 

\section{Concurrence vs parallélisme}

\begin{frame}[t]
  \frametitle{Programmation concurrente}
  \begin{block}{Qu'est-ce que la programmation concurrente ?}
    \begin{itemize}
    \item C'est un {\bf paradigme de programmation}\;;
    \item Autres paradigmes: programmation impérative, fonctionnelle, etc\;;
    \item Souvent confondu avec le {\bf parallélisme}\;;
    \end{itemize}
  \end{block}

  \begin{block}{Programmation concurrente vs parallélisme}
    \begin{itemize}
    \item La concurrence est un concept algorithmique\;;
    \item Certains algorithmes sont intrinsèquement concurrents\;;
    \item \dots d'autres non !
    \item Le parallélisme est un choix d'implémentation\;;
    \item Un algorithme concurrent peut être implémenté de manière purement séquentielle\;;
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[t]
  \begin{center}
    \includegraphics[scale=0.15]{concurrency-vs-parallelism.png}
  \end{center}
\end{frame}

\begin{frame}[t]
  \frametitle{Concurrence en Python}
  \begin{block}{Deux philosophies très différentes:}
    \begin{itemize}
    \item En anglais: {\bf Process} {\em vs.} {\bf Thread}\;;
    \item En français: {\bf Processus} {\em vs.} {\bf Processus léger}\;;
    \item Avec python: module {\tt multiprocessing} {\em vs.} module
      {\tt threading}\;;
    \end{itemize}
  \end{block}

  \begin{block}{Avantages et inconvénients du parallélisme}
    \begin{itemize}
    \item Meilleure utilisation des ressources sur les ordinateurs modernes (multi-c\oe
      urs/processeurs)\;;
    \item Difficultés algorithmiques: tous les algorithmes ne se plient pas
      facilement à la concurrence;;
    \item Difficultés logistiques: {\bf coordination} entre processus\;;
    \item Difficultés de mise-au-point: problèmes liés à la {\bf
        synchronisation} des accès aux données\;;
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[t]
  \frametitle{Module threading}
  \begin{block}{Avantages et inconvénients des threads}
    \begin{itemize}
    \item Tous les threads s'exécutent à l'intérieur d'un unique processus
      python\;;
    \item Par conséquent: tous les threads s'exécutent sur un seul c\oe ur de la machine\;;
    \item Les temps d'exécution sont découpés et répartis par l'interpréteur\;;
    \item Problème du {\bf GIL} (Global Interpreter Lock)\;;
    \item Quand utiliser les threads : lorsqu'il y a un goulot d'étranglement pire
      que le GIL. Par exemple pour la programmation réseau\;;
    \item Quand ne pas utiliser les threads: lorsque l'on veut paralléliser des
      calculs intensifs (pour le processeur)\;;
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[t]
  \frametitle{Module multiprocessing}
  \begin{block}{Avantages et inconvénients des processus}
    \begin{itemize}
    \item Chaque processus est son propre interpréteur python, géré directement
      par l'OS\;;
    \item Utilisation optimale des c\oe urs disponibles\;;
    \item Par contre: lourdeur pour gérer la création de nouveaux processus
      (surtout sous windows). 
    \item Difficulté liée à la communication entre processus: fichiers, pipes,
      sockets\;;
    \item Une bonne règle empirique: Lancer 1 ou 2 processus par c\oe ur disponible\;;
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[t]
  \frametitle{Programme {\tt 01-parallélisme.py}}

  L'objectif est de calculer une somme d'entiers consécutifs à l'aide
  de plusieurs processus en parallèle. En guise de validation, on peut
  utiliser la formule bien connue:
  \vspace{-3mm}
  $$1 + 2 + \cdots + n = \frac{n(n+1)}2$$

  \begin{block}{Caractéristiques du programme}
    \begin{itemize}
    \item Utilisation du module {\tt threading}\;;
      
    \item Création d'une sous-classe de {\tt Thread}\;;

    \item Plusieurs exécutions du programme donnent le même résultat,
      {\em mais pas dans le même ordre}\;;

      \begin{center}
        \fbox{\fboxsep=2mm
          La parallélisme est {\bf non-déterministe}}
      \end{center}
      
    \item L'addition et l'affichage par {\tt print} sont des opérations {\bf
        atomiques}\;;
    \end{itemize}
  \end{block}
\end{frame}

\section{Non-déterminisme}

\begin{frame}[t]
  \frametitle{Programme {\tt 02-non-déterminisme.py}}
  
  \begin{itemize}
  \item On brise l'atomicité de {\tt print} en affichant caractère par
    caractère\;;
  \item Le caractère non-déterministe est très amplifié\;;
  \item Que ce passerait-il si l'addition elle-même était non-atomique
    ?
  \item Que ce passerait-il si plusieurs processus modifient un tableau
    en même temps ?
  \item Comment remettre de l'ordre si on ne peut pas modifier les
    processus légers eux-mêmes ?
  \item Ces problèmes mettent en évidence la nécessité d'un moyen de
    {\bf synchroniser} les accès aux données par différents processus.
  \end{itemize}
\end{frame}

\section{Synchronisation des accès}

\begin{frame}[t]
  \frametitle{Comment synchroniser les accès aux données ?}

  \begin{itemize}
  \item Utilisation de verrous ({\bf locks}) ou {\bf mutex} (pour {\bf
      mu}tuellement {\bf ex}clusif)\;;

  \item Utilisation de {\bf sémaphores}\;;
    
  \item Utilisation d'un langage de programmation spécialement adapté
    au parallélisme (voir le langage spécialisé {\bf clojure})

  \item Avec python: Le module {\tt threading} propose des objets de
    type {\tt Lock}, {\tt Semaphore}, mais aussi {\tt Condition} (pour
    observer les modifications sur une donnée)\;;
    
  \item Enfin, on recommande souvent de synchroniser ses données par
    l'usage d'une ou deux {\bf queues} (aussi appelées {\bf FIFO}),
    qui sont automatiquement synchronisées par python\;;
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Python: module {\tt queue}}

  \begin{itemize}
  \item Une queue sert à stocker des données (quelconques)\;;

  \item {\bf FIFO}: {\bf F}irst {\bf I}n, {\bf F}irst {\bf O}ut. Les
    éléments de la queue sont retirés dans le même ordre que leur
    arrivée dans celle-ci\;;

  \item Les queues sont automatiquement {\bf thread-safe} (résistantes
    aux parallélisme) en python\;;

  \item Les queues sont un moyen simple de regrouper les données
    provenant de {\bf plusieurs} processus parallèles vers {\bf un
      seul} processus coordonnateur\;;
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Programme {\tt 03-synchronization\_par\_une\_queue.py}}

  \begin{itemize}
  \item Les processus (légers) émetteurs sont inchangés\;;

  \item L'affichage de chaque caractère ainsi que les additions sont
    détournés vers une {\tt Queue} globale\;;

  \item Lorsqu'un processus léger a terminé sa tâche, il envoie dans
    la queue une notification de fin de tâche\;;

  \item Le processus principal:
    \begin{itemize}
    \item recueille les données à la sortie de la queue\;;

    \item recrée les lignes d'affichage\;;
      
    \item les affiche lorsqu'elles sont complètes\;;

    \item ajoute les entiers stockés dans la queue\;;

    \item tient à jour les processus qui ont achevés leurs tâches\;;
    \end{itemize}

  \item On constate que la sortie ressemble à celle du premier
    programme: on a recréé l'atomicité de l'affichage.
  \end{itemize}
\end{frame}

\section{Tkinter et parallélisme}

\begin{frame}[t]
  \frametitle{Tkinter et les processus légers}

  \begin{itemize}
  \item Tkinter (comme la plupart des librairies graphiques)
    \underline{n'est pas} {\bf thread-safe} ({\em i.e.} aucune
    garantie de résultat si on accède à Tkinter depuis plusieurs
    processus en parallèle)\;;

  \item Une règle impérative: tous les accès à Tkinter doivent être
    effectués par le processus lançant la boucle principale ({\tt
      mainloop})\;;

  \item On utilise donc une {\tt Queue} pour interfacer de multiples
    processus légers vers Tkinter\;;

  \item Tkinter gère à intervalle régulier la queue et lance lui-même
    les appels vers les procédures graphiques désirées par les
    processus légers\;;
  \end{itemize}
\end{frame}

\begin{frame}[t]
  \frametitle{Programme {\tt 04-Tkinter\_et\_parallélisme.py}}

  \begin{itemize}
  \item Le processus principal crée une {\tt Queue} globale\;;

  \item Chaque processus léger a un lien vers cette queue\;;

  \item Un processus léger gère un disque coloré (création +
    déplacement + gestion des rebonds sur les bords de la fenêtre)\;;

  \item Un processus léger passe par la queue dès lors qu'il souhaite
    accéder à une primitive de Tkinter (en écriture)\;;

  \item Le processus principal rappelle lui-même les méthodes des
    objets associés aux processus légers. Ces méthodes seront alors
    exécutées par le processus principal\;;
  \end{itemize}
\end{frame}

\end{document}
