#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import threading
import time
import queue

# Queue globale, qui sera utilisée par les processus pour synchroniser
# toutes les actions. Notons qu'une structure de queue est
# automatiquement thread-safe (= gère automatiquement la
# synchronisation) en python.
queue_globale = queue.Queue()

# On crée une sous-classe de Thread, dont le but consiste à
# additionner tous les nombre dans un intervalle donné et de placer le
# résultat dans une variable globale partagée.
class ProcessusAdditionneur(threading.Thread):
    def __init__(self, debut, fin):
        threading.Thread.__init__(self)
        self.debut = debut
        self.fin = fin
        self.total = 0

    def run(self):
        # On égrène notre intervalle. Après chaque opération, on
        # attend 10ms afin de pouvoir suivre l'évolution du programme.
        for i in range(self.debut, self.fin+1):

            # Toutes les actions générées par un processus devront
            # être notifiées au programme principal par
            # l'intermédiaire de la queue_globale.
            #
            # Notons que le format des objets stockés dans une queue
            # sont à l'initiative du programmeur.
            #
            # On opte pour la solution suivante. Chaque objet de la
            # queue sera un triplet de la forme
            # (processus, action, valeur)
            #
            # Les différentes possibilités sont:
            #
            # (p, "print", c): on demande à afficher le caractère c
            # (p, "newline", None): un des messages est complet
            # (p, "add", valeur): on demande l'ajout de valeur au
            # total global.
            # (p, "finished", None): la tâche de ce processus est
            # terminée
            #
            chaine = "Processus {}: on ajoute {}".format(self.getName(), i)
            for c in chaine:
                # ATTENTION: on ne peut plus directement effectuer
                # l'affichage d'un caractère.
                #
                #print(c, end="")
                #
                # On va donc utiliser la queue.
                queue_globale.put((self, "print", c))
                time.sleep(0.01)
            queue_globale.put((self, "newline", None))

            # On n'oublie pas d'effectuer le calcul après l'affichage.
            queue_globale.put((self, "add", i))
            time.sleep(0.01)

        # On notifie le processus principal que notre tâche est terminée.
        queue_globale.put((self, "finished", None))

# Tableau servant à garder en mémoire nos processus.
processus = []

# Dans la boucle suivante, on crée puis lance chacun des processus. À
# partir de l'appel à .start(), l'exécution de chaque processus
# s'effectue en parallèle de celle du programme principal.
nombre_processus = 10
intervalle = 10
for i in range(nombre_processus):
    debut = 1 + i*intervalle
    fin = debut + intervalle - 1
    # On crée le processus avec les bons paramètres
    p = ProcessusAdditionneur(debut, fin)
    # On n'oublie pas de garder un lien vers le processus pour
    # référencement ultérieur
    processus.append(p)
    # et enfin, on lance l'éxécution du processus.
    
for i in range(nombre_processus):
    processus[i].start()

total = 0
termine = False
messages = {}
en_cours = {}
for p in processus:
    messages[p] = ""
    en_cours[p] = True

while not termine:
    try:
        p, action, valeur = queue_globale.get(True, 0.01)
        #print(p.getName(), action, valeur)
        if action == "print":
            messages[p] += valeur
        elif action == "newline":
            print(messages[p])
            messages[p] = ""
        elif action == "add":
            total += valeur
        elif action == "finished":
            en_cours[p] = False
            termine = True
            for p in en_cours:
                if en_cours[p] == True:
                    termine = False
    except queue.Empty:
        print("Queue vide !!!!!!")

# Après la boucle précédente, on est certain que tous les processus
# auront terminé leur tâche. On peut donc afficher le résultat.
print("Somme de 1 à {} = {}".format(intervalle*nombre_processus,
                                    total))
