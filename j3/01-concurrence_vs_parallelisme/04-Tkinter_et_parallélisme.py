#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tkinter import *
import threading
import time
import queue
import random
from math import cos, sin


fenetre = Tk()
fenetre.title("Animations avec threads")

largeur = 800
hauteur = 600
canevas = Canvas(fenetre, width=largeur, height=hauteur, bg="white")
canevas.pack(fill=BOTH, expand=YES)

class ProcessusDisque(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)

        # Note importante: la méthode __init__ sera exécutée par le
        # processus principal, puisque le thread créé ici ne sera pas
        # encore lancé par .start().
        #
        # Par conséquent, il n'est pas gênant de créer un nouveau
        # widget.
        #
        # Notons que la boucle principale mainloop() n'est
        # probablement même pas encore lancée pour l'instant.

        self.queue = queue

        # On choisit au hasard une taille et une position de départ.
        self.rayon = random.randint(50, 100)
        self.x = random.randint(0, largeur - self.rayon)
        self.y = random.randint(0, hauteur - self.rayon)
        couleurs = ["red", "green", "blue", "pink", "yellow", "magenta",
                    "cyan", "gray"]
        self.w = canevas.create_oval(self.x, self.y,
                                     self.x + self.rayon,
                                     self.y + self.rayon,
                                     fill=random.choice(couleurs),
                                     outline=random.choice(couleurs))

        # On choisit au hasard une direction de déplacement, ainsi
        # qu'une vitesse symbolisée par le temps d'attente entre deux
        # mise-à-jour de l'affichage.
        self.angle = random.randint(0, 359)
        self.dx = cos(self.angle)
        self.dy = sin(self.angle)
        self.attente = random.random()*0.1

    def run(self):
        # Pour stopper le processus léger, il faudra mettre la
        # variable self.continuer à false. Cela sera effectué par le
        # processus principal (avec l'appel à la méthode .stop())
        # lorsque l'utilisateur souhaitera quitter le programme.
        self.continuer = True
        while self.continuer:
            # On déplace l'objet, tout en gérant les collisions avec
            # les bords de l'écran.
            self.x += self.dx
            self.y += self.dy
            if self.x < 0 or (self.x + self.rayon) > largeur:
                self.dx *= -1
                self.x += self.dx
            if self.y < 0 or (self.y + self.rayon) > hauteur:
                self.dy *= -1

            # Comme on ne peut pas directement changer les coordonnées
            # de l'objet dans le canevas, on empile les requêtes par
            # l'intermédiaire de la queue. Au processus principal de
            # gérer l'affichage proprement dit.
            #
            # Remarque: lorsqu'on ne met pas les parenthèses (),
            # self.affichage n'exécute pas réellement la méthode
            # self.affichage mais renvoie une référence vers cette
            # méthode. Le processus principal pourra alors l'éxécuter
            # en ajoutant les parenthèses. En python (comme dans
            # beaucoup d'autres langages), les parenthèses après un
            # appel de fonctions réalisent l'exécution proprement dite
            # de l'appel.
            self.queue.put(self.affichage)
            time.sleep(self.attente)

    def affichage(self):
        # ATTENTION: cette fonction ne doit jamais être appelée depuis
        # un processus autre que le processus principal (celui qui a
        # lancé mainloop)
        canevas.coords(self.w, self.x, self.y,
                       self.x + self.rayon,
                       self.y + self.rayon)

    def stop(self):
        self.continuer = False

# On crée les processus
processus = []
queue_globale = queue.Queue()
for i in range(256):
    p = ProcessusDisque(queue_globale)
    processus.append(p)

for p in processus:
    p.start()

# Une fonction permettant d'interrompre tous les processus lorsque
# l'on souhaite quitter le programme. Il faut aussi lier l'événement
# de fermeture à l'appel de cette fonction.
def quitter():
    for p in processus:
        p.stop()
    fenetre.destroy()
fenetre.protocol("WM_DELETE_WINDOW", quitter)

# Une fonction, exécutée par le processus principal (plus précisément,
# appelée par mainloop de tkinter). Son rôle est de gérer la queue.
def manage_queue():
    # On dépile tous les événements présents dans la pile
    print("On dépile: ", end="")
    compteur = 0
    while not queue_globale.empty():
        try:
            appel = queue_globale.get(False)
            appel()
            compteur += 1
        except queue.Empty:
            pass
    print("{} événements".format(compteur))
    fenetre.after(10, manage_queue)

#fenetre.after_idle(manage_queue)
fenetre.after_idle(manage_queue)
fenetre.mainloop()
