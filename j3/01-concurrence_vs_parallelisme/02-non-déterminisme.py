#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import threading
import time

total_global = 0

# On crée une sous-classe de Thread, dont le but consiste à
# additionner tous les nombre dans un intervalle donné et de placer le
# résultat dans une variable globale partagée. On affiche un message à
# chaque étape afin de pouvoir suivre l'évolution du programme.
class ProcessusAdditionneur(threading.Thread):
    def __init__(self, debut, fin):
        threading.Thread.__init__(self)
        self.debut = debut
        self.fin = fin
        self.total = 0

    def run(self):
        global total_global

        # On égrène notre intervalle. Après chaque opération, on
        # attend 10ms afin de pouvoir suivre l'évolution du programme.
        for i in range(self.debut, self.fin+1):

            # Contrairement à la version précédente, on affiche le
            # message caractère par caractère, afin de mettre en
            # évidence les problèmes de synchronisation entre
            # processus.
            chaine = "Processus {}: on ajoute {}".format(self.getName(), i)
            for c in chaine:
                print(c, end="")
                time.sleep(0.001)
            print()

            # On n'oublie pas d'effectuer le calcul après l'affichage.
            total_global += i
            time.sleep(0.001)

# Tableau servant à garder en mémoire nos processus.
processus = []

# Dans la boucle suivante, on crée puis lance chacun des processus. À
# partir de l'appel à .start(), l'exécution de chaque processus
# s'effectue en parallèle de celle du programme principal.
nombre_processus = 10
intervalle = 10
for i in range(nombre_processus):
    debut = 1 + i*intervalle
    fin = debut + intervalle - 1
    # On crée le processus avec les bons paramètres
    p = ProcessusAdditionneur(debut, fin)
    # On n'oublie pas de garder un lien vers le processus pour
    # référencement ultérieur
    processus.append(p)
    # et enfin, on lance l'éxécution du processus.
    p.start()

# La boucle précédente est terminée: les processus sont en cours
# d'exécution. Celle-ci n'est pas instantanée à cause de
# time.sleep(s): il faut donc trouver un moyen de suspendre
# temporairement l'exécution de la boucle principale afin de na pas
# afficher le résultat avant que les processus aient terminé leur
# tâches.

for p in processus:
    # La fonction .join() attend que l'exécution d'un processus soit achevée.
    p.join()

# Après la boucle précédente, on est certain que tous les processus
# auront terminé leur tâche. On peut donc afficher le résultat.
print("Somme de 1 à {} = {}".format(intervalle*nombre_processus,
                                    total_global))
