def hanoi(n, a, b, c):
    """Retourne une liste de couple (position_1, position_2) qui s'interprètent
    comme: "déplacer un disque de position_1 vers position_2", où position_i
    peut prendre une des trois valeurs a, b ou c.

    Le nombre de disques n doit être au moins égal à 1, et les trois nombres
    a, b et c des entiers distincts (sous peine d'avoir
    un résultat vide de sens), ou tout autre objet que l'on peut facilement
    distinguer par (des chaînes de caractères par exemple).

    L'appel hanoi(n, a, b, c) a pour but de déplacer n disques depuis le
    plot a vers le plot b, en passant par le plot c:

    >>> hanoi(3, 1, 3, 2)
    [(1, 3), (1, 2), (3, 2), (1, 3), (2, 1), (2, 3), (1, 3)]
    """

    # On utilise l'algorithme récursif classique:
    if n == 1:
        return [(a, b)]
    else:
        return (hanoi(n-1, a, c, b) +
                [(a, b)] +
                hanoi(n-1, c, b, a))


def hanoi_iter(n, a, b, c):
    """hanoi_iter n'est pas une fonction, mais un générateur: sa vocation est
    d'itérer sur les coups permettant de déplacer n disques depuis la position
    a jusqu'à la position b en passant par-dessus la position pivot c.

    L'avantage par rapport à hanoi, est qu'elle n'impose pas d'engendrer la
    liste complète des coups, on peut exploiter ceux-ci au fur et à mesure de
    la progression.

    Pour réaliser cela, on utilise d'une part un algorithme itératif permettant
    d'énumérer les coups de ce jeu, mais aussi le mécanisme des générateurs de
    python, grâce à l'instruction yield qui est une sorte de return temporaire
    (le contexte est sauvegardé jusqu'au prochain appel): lors de l'appel
    suivant à hanoi_iter, c'est comme si la fonction reprenait exactement là où
    elle s'était arrêtée. La syntaxe for de python permet d'utiliser un tel
    générateur de la manière la plus naturelle qui soit:

    >>> for a, b in hanoi_iter(3, 1, 3, 2):
    ...     print(a, "->", b)
    1 -> 3
    1 -> 2
    3 -> 2
    1 -> 3
    2 -> 1
    2 -> 3
    1 -> 3

    Si on souhaite recréer le comportement de hanoi (version récursive
    ci-dessus), on peut procéder à l'aide d'une compréhension de liste:
    >>> [(a, b) for a, b in hanoi_iter(3, 1, 3, 2)]
    [(1, 3), (1, 2), (3, 2), (1, 3), (2, 1), (2, 3), (1, 3)]

    Il est possible d'utiliser HanoiAnimator pour afficher graphiquement le
    déplacement des disques:
    >>> from animator import HanoiAnimator
    >>> ha = HanoiAnimator(3)
    >>> ha.placement_initial()
    >>> for a, b in hanoi_iter(3, 1, 3, 2):
    ...     ha.mouvement(a, b)
    >>> ha.destruction()

    """


    # Une structure contenant tous les disques. Les disques sont numérotés de 0
    # à n-1.
    #
    # Astuce: on ajoute un (n+1)-ième disque (dont le rang sera par conséquent
    # n) au bas de chaque pile, ce disque étant toujours inamovible et
    # permettant d'éviter de tester si une pile est vide (il est toujours
    # possible de poser un des autres disques sur ceux-ci, donc ce
    # disque-plateau joue exactement le même rôle qu'une pile vide).
    slots = {}
    slots[a] = list(reversed(range(n + 1)))
    slots[b] = [n]
    slots[c] = [n]

    def mouvement(p, q):
        """Une fonction interne permettant de déplacer les disques entre deux
        slots. Une vérification de validité est aussi effectuée pour s'assurer
        que l'algorithme utilisé ne comporte aucune erreur manifeste.
        """

        # Le disque n est inamovible, il faut qu'il y ait au moins un autre
        # disque:
        assert(len(slots[p]) > 1)
        # On ne peut pas déplacer un gros disque sur un plus petit:
        assert(slots[p][-1] < slots[q][-1])

        slots[q].append(slots[p].pop())

        # Renvoie le coup joué, ce qui permet d'enchaîner directement avec
        # yield dans la fonction appelante:
        return p, q

    def fini():
        # Le jeu est terminé lorsque les slots a et c ne contiennent plus que
        # le disque inamovible n.
        return len(slots[a]) == 1 and len(slots[c]) == 1

    suivant = {}
    if n % 2 == 1:
        suivant[a] = b
        suivant[b] = c
        suivant[c] = a
    else:
        suivant[a] = c
        suivant[b] = a
        suivant[c] = b

    coup = 0
    petit = a # indique toujours la position du plus petit disque.
    while not fini():
        if coup % 2 == 0:
            # Pour les coups d'ordre pair (ce qui inclut notamment le premier,
            # puisque l'on compte à partir de 0), on déplace forcément le plus
            # petit disque (0), dans la direction donnée par la liste suivant:
            prochain = suivant[petit]
            yield mouvement(petit, prochain)
            petit = prochain
        else:
            # Pour les coups d'ordre impair, on doit nécessairement bouger un
            # autre disque. Il ne peut y avoir qu'un seul mouvement valide dans
            # ce cas:

            # On cherche les deux slots ne contenant pas le petit disque:
            slot1 = suivant[petit]
            slot2 = suivant[slot1]

            # Puis on cherche lequel contient le plus grand disque, et on
            # effectue le mouvement adéquat:
            if slots[slot1][-1] > slots[slot2][-1]:
                yield mouvement(slot2, slot1)
            else:
                yield mouvement(slot1, slot2)

        coup += 1

if __name__ == '__main__':
    import doctest
    doctest.testmod()
