import multiprocessing as mp

def coupe(liste):
   """
   Renvoie 2 sous-listes de tailles égales (à un élément près),
   contenant tous les éléments de 'liste'.
   """

   m = len(liste) // 2
   return liste[:m], liste[m:]

def fusion(l1, l2):
   """
   Fusionne les 2 listes *triées* l1 et l2 en une unique liste,
   elle aussi triée. La liste résultat (supposée vide au 
   départ) est modifiée en place pour contenir le résultat
   de la fusion. 

   Ne renvoie aucun résultat significatif.
   """

   N1 = len(l1)
   N2 = len(l2)
   liste = []
   i1 = 0
   i2 = 0
   while i1 < N1 or i2 < N2:
      if i1 == N1:
         # La liste 1 est épuisée
         liste.append(l2[i2])
         i2 += 1
      elif i2 == N2:
         # La liste 2 est épuisée
         liste.append(l1[i1])
         i1 += 1      
      else:
         if l1[i1] < l2[i2]:
            liste.append(l1[i1])
            i1 += 1      
         else:
            liste.append(l2[i2])
            i2 += 1

   return liste

def tri_fusion(liste, file=None, profondeur=0):
   if len(liste) <= 1:
      # Une liste avec au plus 1 élément est déjà triée
      if file is None:
         return liste
      else:
         file.put(liste)
   else:
      l1, l2 = coupe(liste)
      if profondeur > 0:
         q1 = mp.Queue()
         q2 = mp.Queue()
         # On n'a pas encore atteint la profondeur maximale,
         # on va utiliser le multi-threading
         p1 = mp.Process(target=tri_fusion,
                              args=[l1, q1, profondeur-1])
         p2 = mp.Process(target=tri_fusion,
                              args=[l2, q2, profondeur-1])
         p1.start()
         p2.start()
         r1 = q1.get()
         r2 = q2.get()
         p1.join()
         p2.join()
      else:
         r1 = tri_fusion(l1)
         r2 = tri_fusion(l2)

      if file is None:
         return fusion(r1, r2)
      else:
         file.put(fusion(r1, r2))

if __name__ == '__main__':
   from time import time
   from random import randint

   N = 10000000
   liste = [randint(-N, N) for _ in range(N)]
   
   début = time()
   résultat = tri_fusion(liste, profondeur=4)
   fin = time()

   assert sorted(liste) == résultat

   print("Pour {} éléments, le tri a duré {} secondes.".format(N, fin - début))
   