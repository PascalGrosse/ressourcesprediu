def coupe(liste):
   """
   Renvoie 2 sous-listes de tailles égales (à un élément près),
   contenant tous les éléments de 'liste'.
   """

   m = len(liste) // 2
   return liste[:m], liste[m:]

def fusion(l1, l2):
   """
   Fusionne les 2 listes *triées* l1 et l2 en une unique liste,
   elle aussi triée.
   """

   N1 = len(l1)
   N2 = len(l2)
   liste = []
   i1 = 0
   i2 = 0
   while i1 < N1 or i2 < N2:
      if i1 == N1:
         # La liste 1 est épuisée
         liste.append(l2[i2])
         i2 += 1
      elif i2 == N2:
         # La liste 2 est épuisée
         liste.append(l1[i1])
         i1 += 1      
      else:
         if l1[i1] < l2[i2]:
            liste.append(l1[i1])
            i1 += 1      
         else:
            liste.append(l2[i2])
            i2 += 1
   return liste

def tri_fusion(liste):
   if len(liste) <= 1:
      # Une liste avec au plus 1 élément est déjà triée
      return liste
   else:
      l1, l2 = coupe(liste)
      return fusion(tri_fusion(l1), tri_fusion(l2))

if __name__ == '__main__':
   from time import time
   from random import randint

   N = 10000
   liste = [randint(-N, N) for _ in range(N)]
   
   début = time()
   résultat = tri_fusion(liste)
   #résultat = sorted(liste) # On utilise l'algo de python
   fin = time()

   # On s'assure que le tableau est bien trié
   for i in range(len(résultat) - 1):
      assert résultat[i] <= résultat[i+1]

   print("Pour {} éléments, le tri a duré {} secondes.".format(N, fin - début))
   