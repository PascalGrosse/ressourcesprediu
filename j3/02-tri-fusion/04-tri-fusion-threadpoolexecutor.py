from concurrent.futures import ThreadPoolExecutor

def coupe(liste):
   """
   Renvoie 2 sous-listes de tailles égales (à un élément près),
   contenant tous les éléments de 'liste'.
   """

   m = len(liste) // 2
   return liste[:m], liste[m:]

def fusion(l1, l2):
   """
   Fusionne les 2 listes *triées* l1 et l2 en une unique liste,
   elle aussi triée. 
   """

   résultat = []
   N1 = len(l1)
   N2 = len(l2)
   i1 = 0
   i2 = 0
   while i1 < N1 or i2 < N2:
      if i1 == N1:
         # La liste 1 est épuisée
         résultat.append(l2[i2])
         i2 += 1
      elif i2 == N2:
         # La liste 2 est épuisée
         résultat.append(l1[i1])
         i1 += 1      
      else:
         if l1[i1] < l2[i2]:
            résultat.append(l1[i1])
            i1 += 1      
         else:
            résultat.append(l2[i2])
            i2 += 1
            
   return résultat

def tri_fusion(liste, profondeur=0):
   if len(liste) <= 1:
      # Une liste avec au plus 1 élément est déjà triée
      return liste
   else:
      l1, l2 = coupe(liste)
      if profondeur > 0:
         with ThreadPoolExecutor(max_workers=2) as exec:
            future1 = exec.submit(tri_fusion, l1, profondeur-1)
            future2 = exec.submit(tri_fusion, l2, profondeur-1)
            return fusion(future1.result(), future2.result())
      else:
         return fusion(tri_fusion(l1), tri_fusion(l2))

if __name__ == '__main__':
   from time import time
   from random import randint

   N = 1000000
   liste = [randint(-N, N) for _ in range(N)]
   
   début = time()
   résultat = []
   résultat = tri_fusion(liste, 3)
   fin = time()

   assert sorted(liste) == résultat

   print("Pour {} éléments, le tri a duré {} secondes.".format(N, fin - début))
   