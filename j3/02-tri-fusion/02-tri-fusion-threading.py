import threading

def coupe(liste):
   """
   Renvoie 2 sous-listes de tailles égales (à un élément près),
   contenant tous les éléments de 'liste'.
   """

   m = len(liste) // 2
   return liste[:m], liste[m:]

def fusion(l1, l2, résultat):
   """
   Fusionne les 2 listes *triées* l1 et l2 en une unique liste,
   elle aussi triée. La liste résultat (supposée vide au 
   départ) est modifiée en place pour contenir le résultat
   de la fusion. 

   Ne renvoie aucun résultat significatif.
   """

   N1 = len(l1)
   N2 = len(l2)
   i1 = 0
   i2 = 0
   while i1 < N1 or i2 < N2:
      if i1 == N1:
         # La liste 1 est épuisée
         résultat.append(l2[i2])
         i2 += 1
      elif i2 == N2:
         # La liste 2 est épuisée
         résultat.append(l1[i1])
         i1 += 1      
      else:
         if l1[i1] < l2[i2]:
            résultat.append(l1[i1])
            i1 += 1      
         else:
            résultat.append(l2[i2])
            i2 += 1

def tri_fusion(liste, résultat, profondeur=0):
   if len(liste) <= 1:
      # Une liste avec au plus 1 élément est déjà triée
      résultat.extend(liste)
   else:
      l1, l2 = coupe(liste)
      r1 = []
      r2 = []
      if profondeur > 0:
         # On n'a pas encore atteint la profondeur maximale,
         # on va utiliser le multi-threading
         thread1 = threading.Thread(target=tri_fusion,
                                    args=[l1, r1, profondeur-1])
         thread2 = threading.Thread(target=tri_fusion,
                                    args=[l2, r2, profondeur-1])
         thread1.start()
         thread2.start()
         thread1.join()
         thread2.join()
      else:
         tri_fusion(l1, r1)
         tri_fusion(l2, r2)
         
      fusion(r1, r2, résultat)

if __name__ == '__main__':
   from time import time
   from random import randint

   N = 10000
   liste = [randint(-N, N) for _ in range(N)]
   
   début = time()
   résultat = []
   tri_fusion(liste, résultat, 4)
   fin = time()

   assert sorted(liste) == résultat

   print("Pour {} éléments, le tri a duré {} secondes.".format(N, fin - début))
   