var resetQuestion = function(id) {
    // Réinitialise les checkboxes ou les boutons radio pour une question
    // donnée.

    // On recherche tous les éléments du DOM dont le nom est id:
    var elements = document.getElementsByName(id);
    for (var i in elements) {
        var elt = elements[i];

        // Parmi ceux-ci, on décoche tous les champs input (qui sont forcément
        // du type bouton radio ou checkbox si la page html respecte le cahier
        // des charges).
        if (elt.tagName == 'INPUT') {
            elt.checked = false;
        }
    }
};

var reponsesCochees = function(elements) {
    // Teste les différents élément de type "INPUT" fournis dans la liste
    // elements et recherche ceux qui sont cochés.
    //
    // Renvoie la liste des réponses cochées (du type 'q2b', etc)

    var resultat = [];
    for (var i in elements) {
        var elt = elements[i];
        if (elt.tagName == 'INPUT' && elt.checked) {
            resultat.push(elt.value);
        }
    }

    return resultat;
};

var notifieReponseIncorrecte = function(id) {
    // Lorsqu'une réponse est incorrecte, la faire apparaître en rouge sur la
    // page web.

    var elt = document.getElementById(id);
    elt.style.border = "3px dashed red";
    elt.style.backgroundColor = "#FF9999";
};

var resultatUnique = function(question, reponses, correction, resultats) {
    // Utiliser cette fonction pour les questions n'ayant qu'une seule réponse
    // possible.

    // Teste si la liste des réponses fournies correspond à l'unique bonne
    // réponse donnée par le paramètre correction.

    // Pour une question à choix unique, on part du principe que l'on a utilisé
    // des boutons radio, donc il ne peut y avoir qu'une seule bonne réponse
    // cochée (ou aucune).

    // Cette fonction ne renvoie aucune valeur, mais modifie le dictionnaire
    // resultats qui contient le nombre de réponses absentes, correctes ou
    // incorrectes pour l'ensemble du questionnaire.

    if (reponses.length == 0) {
        resultats["réponses absentes"] += 1;
        notifieReponseIncorrecte(question);
    } else {
        if (reponses.length == 1) {
            if (reponses[0] == correction) {
                resultats["réponses correctes"] += 1;
            } else {
                resultats["réponses fausses"] += 1;
                notifieReponseIncorrecte(question);
            }
        } else {
            // On ne devrait jamais arriver ici... normalement...
            throw new Error("Plus d'une réponse cochée pour une question à choix unique. N'avez-vous pas utilisé de boutons radio ?");
        }
    }
};

var resultatMultiple = function(question, reponses, corrections, resultats) {
    // Utiliser cette fonction pour les questions ayant plusieurs réponses
    // possibles.

    // Compare les réponses fournies (plusieurs réponses possibles) avec celles
    // données dans le tableau corrections. La réponse n'est validée qu'en cas
    // d'égalité parfaite.

    // Cette fonction ne renvoie aucune valeur, mais modifie le dictionnaire
    // resultats qui contient le nombre de réponses absentes, correctes ou
    // incorrectes pour l'ensemble du questionnaire.

    // On part du principe qu'il doit forcément y avoir au moins une réponse à
    // cocher: si ce n'est pas le cas, rajouter une réponse du type "Aucune des
    // réponses précédentes". Cela permet de distinguer une réponse vide
    // (incorrecte par essence) d'une réponse correcte où tous les choix
    // seraient faux.

    if (reponses.length == 0) {
        resultats["réponses absentes"] += 1;
        notifieReponseIncorrecte(question);
    } else if (reponses.length != corrections.length) {
        // Le nombre de réponses n'est pas correct: on ne cherche même pas à
        // savoir lesquelles sont éventuellement justes et on invalide
        // globalement cette question.
        resultats["réponses fausses"] += 1;
        notifieReponseIncorrecte(question);
    } else {
        // Le nombre de réponses correspond, il reste à les comparer avec la
        // correction. Comme on sait que les effectifs correspondent, on peut se
        // contenter de tester l'inclusion simple entre les deux tableaux plutôt
        // que la double inclusion.
        valide = true;
        for (var i in reponses) {
            reponse = reponses[i];
            if (!corrections.includes(reponse)) {
                // Une des réponses au moins est fausse: on invalide toute la
                // question.
                valide = false;
            }
        }

        // Maintenant que l'on a comparé toutes les réponses proposées, il ne
        // reste qu'à conclure:
        if (valide) {
            resultats["réponses correctes"] += 1;
        } else {
            resultats["réponses fausses"] += 1;
            notifieReponseIncorrecte(question);
        }
    }
};

var resetStyles = function() {
    // On réinitialise les styles (ceux-ci ont pu être modifiés lorsque des
    // erreurs ont été notifiées à l'utilisateur):

    questions = document.getElementsByClassName("question");
    for (var i in questions) {
        q = questions[i];

        // On recherche tous les div ayant pour nom "question":
        if (q.tagName == "DIV") {
            // En effacement les attributs style, l'élément reprendra
            // automatiquement les attributs spécifiés dans la feuille css
            // associée à la page.
            q.style.backgroundColor = "";
            q.style.border = "";
        }
    }
};

var afficheScore = function(score, total) {
    // Affiche le score. Plutôt que d'utiliser un simple message d'alerte, on
    // utilise un système plus sophistiqué:
    //
    // L'un des div de la page est invisible par défaut. Lorsque l'on souhaite
    // afficher le score, on le rend visible et on s'arrange pour qu'il soit
    // centré au milieu de l'écran, par-dessus le reste de la page.

    // On rajoute une boîte semi-transparente par-dessus la page: cela aura pour
    // effet d'assombrir celle-ci, tout en permettant la détection des clics en
    // dehors de la boîte de score (afin de refermer celle-ci):
    var modalFond = document.getElementsByClassName("modal-fond")[0];
    // On rend la boîte modale visible, en changeant l'attribut display de
    // "none" vers "block":
    modalFond.style.display = "block";

    var modalBoite = document.getElementsByClassName("modal-boite")[0];
    if (score == total) {
        // Score parfait: on est en vert
        modalBoite.style.backgroundColor = "#88EE88";
        modalBoite.style.border = "3px solid darkgreen";
    } else {
        // Score perfectible: du rouge
        modalBoite.style.backgroundColor = "#FF9999";
        modalBoite.style.border = "3px solid red";
    }

    var resultat = document.getElementById("resultat");
    resultat.innerHTML = "Votre score pour ce test est de " + score + "/" + total + ".";

    // Deux méthodes pour refermer la boîte: clicker sur la croix (un simple
    // x)...
    var closeButton = document.getElementsByClassName("modal-close")[0];
    closeButton.onclick = function() {
        // Pour effacer la boîte de score, on remet l'attribut style.display à
        // "none":
        modalFond.style.display = "none";
    };

    // ... ou bien clicker sur le fond semi-transparent à l'extérieur de la
    // boîte mais au-dessus du reste de la page web.
    window.onclick = function (event) {
        if (event.target == modalFond)
            modalFond.style.display = "none";
    };
};

var envoiReponses = function() {
    // On s'assure que les styles soient dans l'état initial (au cas où cette
    // fonction aurait déjà été appelée auparavant et aurait notifié
    // d'éventuelles erreurs)
    resetStyles();

    // Barème à appliquer. On utilise le barème officiel pour les QCM en NSI:
    var reponseCorrecte = 3;
    var reponseFausse = -1;
    var reponseAbsente = 0;

    // Ce dictionnaire comptera le nombre de réponses correctes ou non, et aussi
    // les réponses absentes.
    resultats = {
        "réponses correctes": 0,
        "réponses fausses": 0,
        "réponses absentes": 0
    };

    // On teste la question 1. Bonne réponce: c
    reponses = reponsesCochees(document.getElementsByName('q1'));
    // Les quatre premières réponses n'ont qu'une seule bonne réponse possible:
    resultatUnique('q1', reponses, 'q1c', resultats);

    // On teste la question 2. Bonne réponse: b
    reponses = reponsesCochees(document.getElementsByName('q2'));
    resultatUnique('q2', reponses, 'q2b', resultats);

    // On teste la question 3. Bonne réponse: d
    reponses = reponsesCochees(document.getElementsByName('q3'));
    resultatUnique('q3', reponses, 'q3d', resultats);

    // On teste la question 4. Bonne réponse: a
    reponses = reponsesCochees(document.getElementsByName('q4'));
    resultatUnique('q4', reponses, 'q4a', resultats);

    // On teste la question 5. Bonnes réponses: a, b
    reponses = reponsesCochees(document.getElementsByName('q5'));
    // La question 5 est la seule pouvant comporter plusieurs réponses
    // correctes, on n'appelle donc pas la même fonction:
    resultatMultiple('q5', reponses, ['q5a', 'q5b'], resultats);

    score = reponseCorrecte*resultats["réponses correctes"];
    score += reponseFausse*resultats["réponses fausses"];
    score += reponseAbsente*resultats["réponses absentes"];

    // On ne veut pas de note finale négative:
    if (score < 0) 
        score = 0;

    var scoreMax = 5 * reponseCorrecte;
    afficheScore(score, scoreMax);
};
