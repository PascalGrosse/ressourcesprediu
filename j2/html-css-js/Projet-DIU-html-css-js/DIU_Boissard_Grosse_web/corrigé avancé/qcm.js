// Pour éviter de polluer l'espace de nom de l'objet window (dans lequel les
// fonctions sont créées par défaut), on crée un dictionnaire qui contiendra à
// la fois toutes les fonctions, mais aussi certaines données "globales".

var qcm = {};

qcm.resetQuestion = function(id) {
    // Réinitialise les checkboxes ou les boutons radio pour une question
    // donnée.

    // On recherche tous les éléments du DOM dont le nom est id:
    var elements = document.getElementsByName(id);
    for (var i in elements) {
        var elt = elements[i];

        // Parmi ceux-ci, on décoche tous les champs input (qui sont forcément
        // du type bouton radio ou checkbox si la page html respecte le cahier
        // des charges).
        if (elt.tagName == 'INPUT') {
            elt.checked = false;
        }
    }
};

qcm.reponsesCochees = function(idQuest) {
    var elements = document.getElementsByName(idQuest);
    // Teste les différents élément de type "INPUT" fournis dans la liste
    // elements et recherche ceux qui sont cochés.
    //
    // Renvoie la liste des réponses cochées (du type 'q2b', etc)

    var resultat = [];
    for (var i in elements) {
        var elt = elements[i];
        if (elt.tagName == 'INPUT' && elt.checked) {
            resultat.push(elt.value);
        }
    }

    return resultat;
};

qcm.notifieReponseIncorrecte = function(id) {
    // Lorsqu'une réponse est incorrecte, la faire apparaître en rouge sur la
    // page web.

    var elt = document.getElementById(id);
    elt.style.border = "3px dashed red";
    elt.style.backgroundColor = "#FF9999";
};

qcm.resultatUnique = function(idQuest, reponses, corrections) {
    // Utiliser cette fonction pour les questions n'ayant qu'une seule réponse
    // possible.

    // Teste si la liste des réponses fournies correspond à l'unique bonne
    // réponse donnée par le paramètre corrections.

    // Pour une question à choix unique, on part du principe que l'on a utilisé
    // des boutons radio, donc il ne peut y avoir qu'une seule bonne réponse
    // cochée (ou aucune).

    // Cette fonction ne renvoie aucune valeur, mais modifie le dictionnaire
    // qcm.resultats qui contient le nombre de réponses absentes, correctes ou
    // incorrectes pour l'ensemble du questionnaire.

    if (reponses.length == 0) {
        qcm.resultats.absentes += 1;
        qcm.notifieReponseIncorrecte(idQuest);
    } else {
        if (reponses.length == 1) {
            if (reponses[0] == corrections) {
                qcm.resultats.correctes += 1;
            } else {
                qcm.resultats.fausses += 1;
                qcm.notifieReponseIncorrecte(idQuest);
            }
        }
    }
};

qcm.resultatMultiple = function(idQuest, reponses, corrections) {
    // Utiliser cette fonction pour les questions ayant plusieurs réponses
    // possibles.

    // Compare les réponses fournies (plusieurs réponses possibles) avec celles
    // données dans le tableau corrections. La réponse n'est validée qu'en cas
    // d'égalité parfaite.

    // Cette fonction ne renvoie aucune valeur, mais modifie le dictionnaire
    // qcm.resultats qui contient le nombre de réponses absentes, correctes ou
    // incorrectes pour l'ensemble du questionnaire.

    // On part du principe qu'il doit forcément y avoir au moins une réponse à
    // cocher: si ce n'est pas le cas, rajouter une réponse du type "Aucune des
    // réponses précédentes". Cela permet de distinguer une réponse vide
    // (incorrecte par essence) d'une réponse correcte où tous les choix
    // seraient faux.

    if (reponses.length == 0) {
        qcm.resultats.absentes += 1;
        qcm.notifieReponseIncorrecte(idQuest);
    } else if (reponses.length != corrections.length) {
        // Le nombre de réponses n'est pas correct: on ne cherche même pas à
        // savoir lesquelles sont éventuellement justes et on invalide
        // globalement cette question.
        qcm.resultats.fausses += 1;
        qcm.notifieReponseIncorrecte(idQuest);
    } else {
        // Le nombre de réponses correspond, il reste à les comparer avec la
        // correction. Comme on sait que les effectifs correspondent, on peut se
        // contenter de tester l'inclusion simple entre les deux tableaux plutôt
        // que la double inclusion.
        var valide = true;
        for (var i in reponses) {
            var reponse = reponses[i];
            if (!corrections.includes(reponse)) {
                // Une des réponses au moins est fausse: on invalide toute la
                // question.
                valide = false;
            }
        }

        // Maintenant que l'on a comparé toutes les réponses proposées, il ne
        // reste qu'à conclure:
        if (valide) {
            qcm.resultats.correctes += 1;
        } else {
            qcm.resultats.fausses += 1;
            qcm.notifieReponseIncorrecte(idQuest);
        }
    }
};

qcm.resetStyles = function() {
    // On réinitialise les styles (ceux-ci ont pu être modifiés lorsque des
    // erreurs ont été notifiées à l'utilisateur):

    var questions = document.getElementsByClassName("question");
    for (var i in questions) {
        var quest = questions[i];

        // On recherche tous les div ayant pour nom "question":
        if (quest.tagName == "DIV") {
            // En effacement les attributs style, l'élément reprendra
            // automatiquement les attributs spécifiés dans la feuille css
            // associée à la page.
            quest.style.backgroundColor = "";
            quest.style.border = "";
        }
    }
};

qcm.afficheScore = function(score, total) {
    // Affiche le score. Plutôt que d'utiliser un simple message d'alerte, on
    // utilise un système plus sophistiqué:
    //
    // L'un des div de la page est invisible par défaut. Lorsque l'on souhaite
    // afficher le score, on le rend visible et on s'arrange pour qu'il soit
    // centré au milieu de l'écran, par-dessus le reste de la page.

    // On rajoute une boîte semi-transparente par-dessus la page: cela aura pour
    // effet d'assombrir celle-ci, tout en permettant la détection des clics en
    // dehors de la boîte de score (afin de refermer celle-ci):
    var modalFond = document.getElementsByClassName("modal-fond")[0];
    // On rend la boîte modale visible, en changeant l'attribut display de
    // "none" vers "block":
    modalFond.style.display = "block";

    var modalBoite = document.getElementsByClassName("modal-boite")[0];
    if (score == total) {
        // Score parfait: on est en vert
        modalBoite.style.backgroundColor = "#88EE88";
        modalBoite.style.border = "3px solid darkgreen";
    } else {
        // Score perfectible: du rouge
        modalBoite.style.backgroundColor = "#FF9999";
        modalBoite.style.border = "3px solid red";
    }

    var resultat = document.getElementById("resultat");
    resultat.innerHTML = "Votre score pour ce test est de " + score + "/" + total + ".";

    // Deux méthodes pour refermer la boîte: clicker sur la croix (un simple
    // x)...
    var closeButton = document.getElementsByClassName("modal-close")[0];
    closeButton.onclick = function() {
        // Pour effacer la boîte de score, on remet l'attribut style.display à
        // "none":
        modalFond.style.display = "none";
    };

    // ... ou bien clicker sur le fond semi-transparent à l'extérieur de la
    // boîte mais au-dessus du reste de la page web.
    window.onclick = function (event) {
        if (event.target == modalFond)
            modalFond.style.display = "none";
    };
};

qcm.envoiReponses = function() {
    // Compare les réponses fournies par l'utilisateur avec les corrections, et
    // affiche le bilan.

    // On s'assure que les styles soient dans l'état initial (au cas où cette
    // fonction aurait déjà été appelée auparavant et aurait notifié
    // d'éventuelles erreurs)
    qcm.resetStyles();

    // Barème à appliquer. On utilise le barème officiel pour les QCM en NSI:
    var reponseCorrecte = 3;
    var reponseFausse = -1;
    var reponseAbsente = 0;

    // Ce dictionnaire comptera le nombre de réponses correctes ou non, et aussi
    // les réponses absentes.
    qcm.resultats = {};
    qcm.resultats.correctes = 0;
    qcm.resultats.fausses = 0;
    qcm.resultats.absentes = 0;

    // On parcourt l'ensemble des questions possibles, et on compare avec les
    // corrections:
    for (var idQuest in qcm.reponses) {
        // Les réponses spécifiées par le concepteur du questionnaire:
        var listeReponses = qcm.reponses[idQuest];
        // Les réponses du candidat:
        var reponsesCandidat = qcm.reponsesCochees(idQuest);
        if (listeReponses.length == 1)
            qcm.resultatUnique(idQuest, reponsesCandidat, listeReponses[0]);
        else
            qcm.resultatMultiple(idQuest, reponsesCandidat, listeReponses);
    }

    score = reponseCorrecte * qcm.resultats.correctes;
    score += reponseFausse * qcm.resultats.fausses;
    score += reponseAbsente * qcm.resultats.absentes;

    // On ne veut pas de note finale négative:
    if (score < 0) 
        score = 0;

    var scoreMax = 5 * reponseCorrecte;
    qcm.afficheScore(score, scoreMax);
};

qcm.loadMathjax = function() {
    // Trouvé et adapté de la doc officielle de Mathjax

    var script1 = document.createElement("script");
    script1.type = "text/x-mathjax-config";
    script1.innerHTML = "MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});";
    document.getElementsByTagName("head")[0].appendChild(script1);

    var script2 = document.createElement("script");
    script2.src  = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML";
    document.getElementsByTagName("head")[0].appendChild(script2);
};

qcm.loadGooglePrettify = function() {
    // Trouvé et adapté de la doc officielle de google prettify

    var script = document.createElement("script");
    script.src = "https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?lang=css&amp;skin=sunburst";
    document.getElementsByTagName("head")[0].appendChild(script);
};

qcm.idExercice = function(exonum) {
    // Retourne un identifiant unique pour un exercice:
    return "exo" + (parseInt(exonum) + 1);
};

qcm.idQuestion = function(exonum, questnum) {
    // Retourne un identifiant unique pour une question:
    return qcm.idExercice(exonum) + "q" + (parseInt(questnum) + 1);
};

qcm.idReponse = function(exonum, questnum, repnum) {
    // Retourne un identifiant unique pour une réponse:
    return qcm.idQuestion(exonum, questnum) + "_" + (parseInt(repnum) + 1);
};

qcm.creeBouttonEffacement = function(exonum, questnum) {
    // Crée un bouton permettant d'effacer les réponses fourniées pour une
    // question. C'est un luxe pour les checkboxes (quoique ça évite de tout
    // décocher à la main), mais c'est indispensable pour les boutons radio car
    // il n'y a pas moyen de le décocher une fois qu'une alternative a été
    // sélectionnée: or une réponse fausse coûte des points négatifs en général.

    var div = document.createElement("div");
    div.classList.add("reinit");
    var input = document.createElement("input");
    input.setAttribute("type", "button");
    input.setAttribute("name", qcm.idQuestion(exonum, questnum) + "reset");
    input.setAttribute("onclick", "qcm.resetQuestion(\"" + qcm.idQuestion(exonum, questnum) + "\")");
    input.setAttribute("value", "Effacer ma réponse");
    div.appendChild(input);

    return div;
};

qcm.creeTexte = function(texte) {
    // Crée un paragraphe de texte:

    var paragraphe = document.createElement("p");
    paragraphe.innerHTML = texte;

    return paragraphe;
};

qcm.creeReponse = function(texte, idrep) {
    // Crée le label pour un choix donné:

    var label = document.createElement("label");
    label.setAttribute("for", idrep);
    var span = document.createElement("span");
    span.classList.add("reponse");
    span.innerHTML = texte;
    label.appendChild(span);
    return label;
};

qcm.questionEstAChoixUnique = function(reponses) {
    // Teste si la liste des réponses comporte une unique réponse correcte ou
    // plusieurs. Cela servira notamment à choisir entre des checkboxes et des
    // boutons radio.
    var trueCount = 0;
    for (var repnum in reponses) {
        var rep = reponses[repnum];
        if (rep.correcte == true)
            trueCount += 1;
    }

    if (trueCount == 1)
        return true;
    else
        return false;
};

qcm.creeReponses = function(question, exonum, questnum) {
    //Crée dynamiquement la liste des choix pour une question donnée. Le nombre
    // de colonnes à utiliser (entre 2 et 4) est calculé automatiquement, sauf
    // s'il a été spécifié dans la structure json.

    // On crée une liste de réponses vide (pour l'instant) pour cette question.
    // Rappelons que le dictionnaire qcm.reponses sera utilisé lors du
    // dépouillement des réponses de l'utilisateur, afin de noter les résultats.
    var idQuest = qcm.idQuestion(exonum, questnum);
    qcm.reponses[idQuest] = [];

    var divChoix = document.createElement("div");
    var nbrChoix = question.reponses.length;

    // On calcule le nombre de colonnes nécessaires à l'affichage. C'est au
    // niveau du css que ce nombre sera effectivement géré.
    var colonnes = 4;
    if ("colonnes" in question)
        colonnes = parseInt(question.colonnes);
    else if (nbrChoix >= 2 && nbrChoix <= 4) 
        colonnes = nbrChoix;
    divChoix.classList.add("choix" + colonnes);

    for (var repnum in question.reponses) {
        var rep = question.reponses[repnum];
        var idRep = qcm.idReponse(exonum, questnum, repnum);

        var divUnChoix = document.createElement("div");
        divUnChoix.classList.add("choix");
        var input = document.createElement("input");
        if (qcm.questionEstAChoixUnique(question.reponses) == true)
            input.setAttribute("type", "radio");
        else
            input.setAttribute("type", "checkbox");
        input.setAttribute("name", idQuest);
        input.setAttribute("id", idRep);
        input.setAttribute("value", idRep);
        divUnChoix.appendChild(input);
        divUnChoix.appendChild(qcm.creeReponse(rep.texte,
                                               qcm.idReponse(exonum, questnum, repnum)));
        divChoix.appendChild(divUnChoix);

        // Si la réponse est correcte, on la rajoute à la liste des réponses
        // correctes dans le dictionnaire qcm.reponses:
        if (rep.correcte == true)
            qcm.reponses[idQuest].push(idRep);
    }

    return divChoix;
};

qcm.creeTitreQuestion = function(question, exonum, questnum) {
    // Une question aura toujours au minimum pour titre "Question n°", mais si
    // un titre existe effectivement dans la structure json, il sera alors
    // rajouté ici.

    var paragraph = document.createElement("p");
    exonum = parseInt(exonum) + 1;
    questnum = parseInt(questnum) + 1;
    var spanNum = document.createElement("span");
    spanNum.classList.add("questionnum");
    spanNum.innerHTML = "Question " + exonum + "." + questnum;
    paragraph.appendChild(spanNum);
    if ("titre" in question && question.titre.length > 0) {
        var spanTitre = document.createElement("span");
        spanNum.innerHTML += ": ";
        spanTitre.classList.add("questiontitre");
        spanTitre.innerHTML = question.titre;
        paragraph.appendChild(spanTitre);
    }

    return paragraph;
};

qcm.creeQuestionComplexe = function(question, exonum, questnum) {
    // Crée dynamiquement une question comportant éventuellement un titre et //
    // éventuellement un énoncé, en plus // de la liste des choix possibles.
    // Utilisé lorsqu'un exercice comporte // plusieurs questions.

    var divQuestion = document.createElement("div");
    divQuestion.classList.add("question");
    divQuestion.setAttribute("id", qcm.idQuestion(exonum, questnum));
    divQuestion.appendChild(document.createElement("hr"));
    divQuestion.appendChild(qcm.creeTitreQuestion(question, exonum, questnum));
    if ("enonce" in question && question.enonce.length > 0) {
        divQuestion.appendChild(qcm.creeTexte(question.enonce));
    }
    divQuestion.appendChild(qcm.creeBouttonEffacement(exonum, questnum));
    divQuestion.appendChild(qcm.creeReponses(question, exonum, questnum));

    return divQuestion;
};

qcm.creeQuestionSimple = function(question, exonum) {
    // Crée dynamiquement la question pour un exercice n'en comportant qu'une
    // seule (pas de titre de question, ni de sous-énoncé).

    // On attribue de manière arbitraire un n° de question:
    var questnum = 0;

    var divQuestion = document.createElement("div");
    divQuestion.classList.add("question");
    divQuestion.setAttribute("id", qcm.idQuestion(exonum, questnum));
    divQuestion.appendChild(qcm.creeBouttonEffacement(exonum, questnum));
    divQuestion.appendChild(qcm.creeReponses(question, exonum, questnum));

    return divQuestion;
};

qcm.creeTitreExercice = function(exo, exonum) {
    // Le titre de l'exercice comporte toujours au moins la mention "Exercice
    // n°". De manière facultative, on rajoute un titre effectif s'il existe.

    var legend = document.createElement("legend");
    var numero = parseInt(exonum) + 1;
    var spanNum = document.createElement("span");
    spanNum.classList.add("exercicenum");
    spanNum.innerHTML = "Exercice " + numero;
    legend.appendChild(spanNum);
    if ("titre" in exo && exo.titre.length > 0) {
        var spanTitre = document.createElement("span");
        spanNum.innerHTML += ": ";
        spanTitre.classList.add("exercicetitre");
        spanTitre.innerHTML = exo.titre;
        legend.appendChild(spanTitre);
    }

    return legend;
};

qcm.creeUnExercice = function(exo, exonum) {
    // Crée dynamiquement *un* exercice (le json est passé en paramètre dans
    // exo).

    divExercice = document.createElement("div");
    divExercice.classList.add("exercice");
    divExercice.setAttribute("id", qcm.idExercice(exonum));

    var fieldset = document.createElement("fieldset");
    fieldset.appendChild(qcm.creeTitreExercice(exo, exonum));
    fieldset.appendChild(qcm.creeTexte(exo.enonce));;

    // Il y a deux types d'exercices possibles:
    // 1. Soit on trouve directement un choix de réponses (l'exercice comporte
    //    alors de facto une unique question);
    // 2. Soit on trouve une liste de questions, chacune ayant son propre
    //    sous-énoncé et son jeu de réponses.
    if ("reponses" in exo) {
        // On est dans le premier cas: on ajoute une question qui est un simple
        // jeu de réponses, l'énoncé ayant déjà été placé.
        fieldset.appendChild(qcm.creeQuestionSimple(exo, exonum));
    } else if ("questions" in exo) {
        // On est dans le deuxième cas: il faut créer une liste de questions
        for (var questnum in exo.questions) {
            var quest = exo.questions[questnum];
            fieldset.appendChild(qcm.creeQuestionComplexe(quest, exonum, questnum));
        }
    }

    divExercice.appendChild(fieldset);

    return divExercice;
};

qcm.creeExercices = function() {
    // Cette fonction crée dynamiquement la liste des exercices. Utilise la
    // variable globale qcm.questionnaire comme source de données, au format json.

    // On récupère la référence vers le formulaire...
    var form = document.getElementById("questionnaire");
    // ... et, à l'intérieur de celui-ci, vers le div placé tout à la fin
    // (contenant le bouton pour soumettre les réponses).
    var divEnvoi = document.getElementById("envoi");

    for (var exonum in qcm.questionnaire) {
        // On crée l'exercice...
        exo = qcm.questionnaire[exonum];
        // ...puis on l'insère dans le formulaire, mais avant le bouton d'envoi.
        form.insertBefore(qcm.creeUnExercice(exo, exonum), divEnvoi);
    }
};

qcm.creeQuestionnaire = function() {
    // Le questionnaire complet, au format json. Attention: aucune validation de
    // la structure n'est effectuée dans cette version !

    qcm.questionnaire = [
        {
            "type": "exercice",
            "titre": "Binaire vers décimal",
            "enonce": "Comment s'écrit en décimal le nombre binaire $\\small\\tt{10010100}_2$ ?",
            "reponses": [
                {"texte": "$\\small\\tt{85}_{10}$", "correcte": false},
                {"texte": "$\\small\\tt{111}_{10}$", "correcte": false},
                {"texte": "$\\small\\tt{148}_{10}$", "correcte": true},
                {"texte": "$\\small\\tt{139}_{10}$", "correcte": false}
            ]
        },
        {
            "type": "exercice",
            "titre": "Listes en python",
            "enonce": `
                Voici un programme écrit en Python:
                <pre class="prettyprint linenums lang-python">
                l1 = list(range(3))
                l2 = list(range(10, 13))
                l3 = l1 + l2
                l4 = l1.append(5)</pre>`,
            "questions": [
                {
                    "type": "question",
                    "titre": "Somme de deux listes",
                    "enonce": `
                        Quel est le contenu de la variable
                        <code class="prettyprint lang-python">l3</code>
                        après exécution de la ligne 3 ?`,
                    "colonnes": 2, // par défaut, le nombre de colonnes est
                    // calculé automatiquement, mais il est possible de le
                    // spécifier explicitement ici.
                    "reponses": [
                        {
                            "texte": '<code class="prettyprint lang-python">[10, 12, 14]</code>',
                            "correcte": false
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 10, 11, 12]</code>',
                            "correcte": true
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 3, 10, 11, 12, 13]</code>',
                            "correcte": false
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">None</code>',
                            "correcte": false
                        }
                    ]
                },
                {
                    "titre": "Liste.append (1)",
                    "enonce": `
                        Quel est le contenu de la variable
                        <code class="prettyprint lang-python">l4</code>
                        après exécution de ces quatre lignes ?`,
                    "colonnes": 2,
                    "reponses": [
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 5]</code>',
                            "correcte": false
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 3, 5]</code>',
                            "correcte": false
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 10, 11, 12, 5]</code>',
                            "correcte": false
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">None</code>',
                            "correcte": true
                        }
                    ]
                },
                {
                    "titre": "List.append (2)",
                    "enonce": `
                        Quel est le contenu de la variable
                        <code class="prettyprint lang-python">l1</code>
                        après exécution de ces quatre lignes ?`,
                    "colonnes": 2,
                    "reponses": [
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 5]</code>',
                            "correcte": true
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2]</code>',
                            "correcte": false
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 3, 5]</code>',
                            "correcte": false
                        },
                        {
                            "texte": '<code class="prettyprint lang-python">[0, 1, 2, 3]</code>',
                            "correcte": false
                        }
                    ]
                }
            ]
        },
        {
            "type": "exercice",
            "titre": "Conversion héxadécimal <-> binaire",
            "enonce": "Cochez toutes les égalités vraies:",
            "colonnes": 3,
            "reponses": [
                {"texte": "$\\small\\tt{10001010010}_2 = \\tt{452}_{16}$", "correcte": true},
                {"texte": "$\\small\\tt{C9C}_{16} = \\tt{110010011100}_2$", "correcte": true},
                {"texte": "$\\small\\tt{EF4}_{16} = \\tt{111011111000}_2$", "correcte": false},
                {"texte": "$\\small\\tt{F66}_{16} = \\tt{111101100111}_2$", "correcte": false},
                {"texte": "$\\small\\tt{F2C}_{16} = \\tt{111100101111}_2$", "correcte": false},
                {"texte": "$\\small\\tt{10111100100}_2 = \\tt{5E2}_{16}$", "correcte": false},
                {"texte": "$\\small\\tt{111001111010}_2 = \\tt{E75}_{16}$", "correcte": false},
                {"texte": "$\\small\\tt{E8}_{16} = \\tt{11101100}_2$", "correcte": false},
                {"texte": "Aucune: elles sont toutes fausses !", "correcte": false}
            ]
        }
    ];

    // Le dictionnaire suivant sera automatiquement construit lors de la
    // création dynamique du html. Il associera à chaque identifiant de question
    // la liste des identifiants de réponses correctes: cela sera utilisé pour
    // comparer avec les réponses fournies par l'utilisateur.
    qcm.reponses = {};
    // On crée à présent le html dynamiquement à partir de la structure ci-dessus:
    qcm.creeExercices();
    // À ce stade, le dictionnaire qcm.reponses devrait être correctement
    // initialisé.
    //
    // console.log(qcm.reponses);

    // On doit charger mathjax et google prettify de manière dynamique, parce
    // qu'ils opèrent normalement avec un onload() dans le body, donc c'est trop
    // tard lorsque les exercices sont ajoutées dynamiquement après le
    // chargement de la page. 
    qcm.loadMathjax();
    qcm.loadGooglePrettify();
};
