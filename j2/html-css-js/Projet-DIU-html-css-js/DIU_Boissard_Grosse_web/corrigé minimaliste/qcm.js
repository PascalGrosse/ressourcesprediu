var soumettreReponse = function() {
    var reponse_correcte = 3;
    var reponse_fausse = -1;
    var reponse_absente = 0;

    // On doit parcourir l'ensemble des questions, récupérer la réponse cochée,
    // et calculer le score en fonction de cela.

    // Un dictionnaire associant à chaque question sa bonne réponse:
    var questions = {"q1": "q1c",
                     "q2": "q2a",
                     "q3": "q3a",
                     "q4": "q4d",
                     "q5": "q5a"};
    // En javascript, la syntaxe pour récupérer la taille d'un dictionnaire est
    // pour le moins... compliquée...
    var nombreQuestions = Object.keys(questions).length;

    var score = 0;

    for (var quest in questions) {
        var rep = questions[quest];

        // On récupère tous les éléments du DOM portant le bon nom.
        var elements = document.getElementsByName(quest);

        for (var j = 0; j < elements.length; j++) {
            var elt = elements[j];

            // Parmi ces éléments, on cherche celui ayant un tag input et qui
            // est coché:
            if (elt.tagName == "INPUT" && elt.checked == true) {
                // Trois cas peuvent se produire: 
                if (elt.value == "nullrep")
                    // pas de réponse
                    score += reponse_absente;
                else if (elt.value == rep)
                    // réponse correcte
                    score += reponse_correcte;
                else
                    // réponse incorrecte
                    score += reponse_fausse;
            }
        }
    }

    // On ne veut pas de note finale négative
    if (score < 0)
        score = 0;

    window.alert("Votre score est de " + score + " points sur " +
                 (Object.keys(questions).length * reponse_correcte));
};
