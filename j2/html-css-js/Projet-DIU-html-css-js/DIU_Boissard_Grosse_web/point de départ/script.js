function bienvenue() {
    // La première instruction permet d'obtenir la liste de tous 
    // les éléments qui ont "origine" comme nom.
    var elements = document.getElementsByName("origine");
    var message = "";
    var couleurFond = ""
    var couleurTexte = ""
    
    for (var i = 0; i < elements.length; i++) {
        var elt = elements[i];
        // Parmi ces éléments, on cherche celui ayant un tag input et qui
        // est coché :
        if (elt.tagName == "INPUT" && elt.checked == true) {
            // Trois cas peuvent se produire: 
            if (elt.value == "FR"){
                message = "Vous êtes de nationalité française, soyez le bienvenu.";
                couleurFond = "blue";
                couleurTexte = "white";
            }
            else if (elt.value == "ALL"){
                message = "Vous êtes de nationalité allemande, soyez le bienvenu.";
                couleurFond = "black";
                couleurTexte = "orange";
            }
            else{
                message = "D'où que vous soyez, soyez le bienvenu.";
                couleurFond = "salmon";
                couleurTexte = "black";
            }
        }
    }
    // On met maintenant à jour les attributs de l'objet identifié
    // par l'id "message" (cf code html)
    var paraMessage = document.getElementById("message");
    paraMessage.innerHTML = message;
    paraMessage.style.backgroundColor = couleurFond;
    paraMessage.style.color = couleurTexte;
    
}
