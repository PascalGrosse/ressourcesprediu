from http.server import HTTPServer, BaseHTTPRequestHandler
from io import BytesIO
from urllib.parse import urlparse

from makejson import toutes_les_questions

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(toutes_les_questions().encode("utf-8"))

        # Pour l'instant on ne lit pas les paramètres du GET, on balance toutes
        # les questions sans s'en poser aucune. Mais si on veut le faire,
        # query_components ci-dessous contient le dictionnaire des requêtes de
        # l'URL.

        #query = urlparse(self.path).query
        #query_components = dict(qc.split("=") for qc in query.split("&"))

httpd = HTTPServer(('localhost', 8000), SimpleHTTPRequestHandler)
httpd.serve_forever()
