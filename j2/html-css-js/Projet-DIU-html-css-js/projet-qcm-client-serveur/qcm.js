var donnees = {};

var loadMathjax = function() {
    // Trouvé dans la doc officielle de Mathjax

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src  = "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML"
    document.getElementsByTagName("head")[0].appendChild(script);
}

var loadGooglePrettify = function() {
    // Trouvé et adapté de la doc officielle de google prettify

    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js?lang=css&amp;skin=sunburst";
    document.getElementsByTagName("head")[0].appendChild(script);
}

var idExercice = function(exonum) {
    return "exo" + (parseInt(exonum) + 1);
}

var idQuestion = function(exonum, questnum) {
    return "e" + (parseInt(exonum) + 1) + "q" + (parseInt(questnum) + 1);
}

var idReponse = function(exonum, questnum, repnum) {
    return idQuestion(exonum, questnum) + "_" + (parseInt(repnum) + 1);
}

var creeBouttonEffacement = function(exonum, questnum) {
    var div = document.createElement("div");
    div.classList.add("reinit");
    var input = document.createElement("input");
    input.setAttribute("type", "button");
    input.setAttribute("name", idQuestion(exonum, questnum) + "reset");
    input.setAttribute("onclick", "resetQuestion(" + idQuestion(exonum, questnum) + ")");
    input.setAttribute("value", "Effacer ma réponse");
    div.appendChild(input);

    return div;
}

var creeTexte = function(texte) {
    var paragraphe = document.createElement("p");
    paragraphe.innerHTML = texte;

    return paragraphe;
}

var creeReponse = function(texte) {
    var span = document.createElement("span");
    span.classList.add("reponse");
    span.innerHTML = texte;

    return span
}

var creeChoix = function(question, exonum, questnum) {
    console.log(question, exonum, questnum);
    var divChoix = document.createElement("div");
    var nbrChoix = question.choix.length;
    var colonnes = parseInt(question.colonnes);
    if (colonnes == -1) {
        if (nbrChoix >= 2 && nbrChoix <= 4) {
            colonnes = nbrChoix;
        } else {
            colonnes = 4;
        }
    }
    divChoix.classList.add("choix" + colonnes);
    for (var repnum in question.choix) {
        choix = question.choix[repnum];
        var divUnChoix = document.createElement("div");
        divUnChoix.classList.add("choix");
        var input = document.createElement("input");
        if (question.unique == true)
            input.setAttribute("type", "radio");
        else
            input.setAttribute("type", "checkbox");
        input.setAttribute("name", idQuestion(exonum, questnum));
        input.setAttribute("value", idReponse(exonum, questnum, repnum));
        divUnChoix.appendChild(input);
        divUnChoix.appendChild(creeReponse(choix[0]));
        divChoix.appendChild(divUnChoix);
    }

    return divChoix;
}

var creeTitreQuestion = function(question, exonum, questnum) {
    var paragraph = document.createElement("p");
    exonum = parseInt(exonum) + 1;
    questnum = parseInt(questnum) + 1;
    var spanNum = document.createElement("span");
    spanNum.classList.add("questionnum");
    spanNum.innerHTML = "Question " + exonum + "." + questnum;
    paragraph.appendChild(spanNum);
    if ("nom" in question && question.nom.length > 0) {
        var spanTitre = document.createElement("span");
        spanNum.innerHTML += ": ";
        spanTitre.classList.add("questiontitre");
        spanTitre.innerHTML = question.nom;
        paragraph.appendChild(spanTitre);
    }

    return paragraph;
}

var creeQuestionMultiple = function(question, exonum, questnum) {
    divQuestion = document.createElement("div");
    divQuestion.classList.add("question");
    divQuestion.setAttribute("name", idQuestion(exonum, questnum));
    divQuestion.appendChild(document.createElement("hr"));
    divQuestion.appendChild(creeTitreQuestion(question, exonum, questnum));
    if ("texte" in question && question.texte.length > 0) {
        divQuestion.appendChild(creeTexte(question.texte));
    }
    divQuestion.appendChild(creeBouttonEffacement(exonum, questnum));
    divQuestion.appendChild(creeChoix(question, exonum, questnum));

    return divQuestion;
}

var creeQuestionUnique = function(question, exonum, questnum) {
    divQuestion = document.createElement("div");
    divQuestion.classList.add("question");
    divQuestion.setAttribute("name", idQuestion(exonum, questnum));
    divQuestion.appendChild(creeBouttonEffacement(exonum, questnum));
    divQuestion.appendChild(creeChoix(question, exonum, questnum));

    return divQuestion;
}

var creeTitreExercice = function(exo, exonum) {
    var legend = document.createElement("legend");
    numero = parseInt(exonum) + 1;
    var spanNum = document.createElement("span");
    spanNum.classList.add("exercicenum");
    spanNum.innerHTML = "Exercice " + numero;
    legend.appendChild(spanNum);
    if ("nom" in exo && exo.nom.length > 0) {
        var spanTitre = document.createElement("span");
        spanNum.innerHTML += ": ";
        spanTitre.classList.add("exercicetitre");
        spanTitre.innerHTML = exo.nom;
        legend.appendChild(spanTitre);
    }

    return legend;
};

var creeExercice = function(exo, exonum) {
    divExercice = document.createElement("div");
    divExercice.classList.add("exercice");
    divExercice.setAttribute("name", idExercice(exonum));

    var fieldset = document.createElement("fieldset");
    fieldset.appendChild(creeTitreExercice(exo, exonum));
    fieldset.appendChild(creeTexte(exo.texte));;

    if (exo.questions.length > 1) {
        for (var questnum in exo.questions) {
            var question = exo.questions[questnum];
            fieldset.appendChild(creeQuestionMultiple(question, exonum, questnum));
        }
    } else {
        var quest = exo.questions[0];
        fieldset.appendChild(creeQuestionUnique(quest, exonum, 0));
    }

    divExercice.appendChild(fieldset);

    return divExercice;
};

var creeExercices = function(json) {
    var form = document.getElementById("questionnaire");

    for (var exonum in json) {
        exo = json[exonum];
        form.appendChild(creeExercice(exo, exonum));
    }
}

var chargeExercices = function() {
    var jsonUrl = "http://localhost:8000/";
    var requete = new XMLHttpRequest();
    requete.open("GET", jsonUrl);
    requete.responseType = "json";
    requete.send();

    requete.onload = function() {
        creeExercices(requete.response);
    };

    // On doit charger mathjax et google prettify de manière dynamique, parce
    // qu'ils opèrent normalement avec un onload() dans le body, donc c'est trop
    // tard pour les questions ajoutées après-coup.
    loadMathjax();
    loadGooglePrettify();
}

