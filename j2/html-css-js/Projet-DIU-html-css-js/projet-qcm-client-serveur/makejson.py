import json


def réponse_fausse(texte):
    return [texte, False]


def réponse_vraie(texte):
    return [texte, True]


def question(*choix, nom="", texte="", colonnes=-1, unique=None):
    if unique is None:
        countTrue = 0
        for c in choix:
            if c[1] is True:
                countTrue += 1
        if countTrue == 1:
            unique = True
        else:
            unique = False
    q = {"type": "question", "choix": choix, "colonnes": colonnes, "unique": unique}

    if len(nom) > 0:
        q["nom"] = nom
    if len(texte) > 0:
        q["texte"] = texte

    return q


def exercice(nom, texte, *liste_questions):
    # Le bricolage ci-dessous sert à retirer les blancs en début de ligne pour
    # les chaînes de caractères multilignes de python. Ainsi, pas besoin
    # d'aligner à la marge gauche, ce qui est très laid dans une indentation
    # python.
    texte = "\n".join([s for s in [s.strip() for s in texte.splitlines()] if len(s) > 0])

    return {"type":"exercice", "nom": nom, "texte": texte, "questions": liste_questions}


def toutes_les_questions():
    e1 = exercice("Binaire vers décimal",
                  r"Comment s'écrit en décimal le nombre binaire $\small\tt{10010100}_2$ ?",
                  question(réponse_fausse(r"$\small\tt{85}_{10}$"),
                           réponse_fausse(r"$\small\tt{111}_{10}$"),
                           réponse_vraie(r"$\small\tt{148}_{10}$"),
                           réponse_fausse(r"$\small\tt{139}_{10}$")))

    e2 = exercice("Listes en python",
                  """
                  Voici un programme écrit en Python:
                  <pre class="prettyprint linenums lang-python">
                  l1 = list(range(3))
                  l2 = list(range(10, 13))
                  l3 = l1 + l2
                  l4 = l1.append(5)</pre>""",
                  question(réponse_fausse('<code class="prettyprint lang-python">[10, 12, 14]</code>'),
                           réponse_vraie('<code class="prettyprint lang-python">[0, 1, 2, 10, 11, 12]</code>'),
                           réponse_fausse('<code class="prettyprint lang-python">[0, 1, 2, 3, 10, 11, 12, 13]</code>'),
                           réponse_fausse('<code class="prettyprint lang-python">None</code>'),
                           nom="Somme de deux listes",
                           texte="""
                           Quel est le contenu de la variable
                           <code class="prettyprint lang-python">l3</code>
                           après exécution de la ligne 3 ?"""),
                  question(réponse_fausse('<code class="prettyprint lang-python">[0, 1, 2, 5]</code>'),
                           réponse_fausse('<code class="prettyprint lang-python">[0, 1, 2, 3, 5]</code>'),
                           réponse_fausse('<code class="prettyprint lang-python">[0, 1, 2, 10, 11, 12, 5]</code>'),
                           réponse_vraie('<code class="prettyprint lang-python">None</code>'),
                           nom=r"List.append (1)",
                           texte="""
                           Quel est le contenu de la variable
                           <code class="prettyprint lang-python">l4</code>
                           après exécution de ces quatre lignes ?"""),
                  question(réponse_vraie('<code class="prettyprint lang-python">[0, 1, 2, 5]</code>'),
                           réponse_fausse('<code class="prettyprint lang-python">[0, 1, 2]</code>'),
                           réponse_fausse('<code class="prettyprint lang-python">[0, 1, 2, 3, 5]</code>'),
                           réponse_fausse('<code class="prettyprint lang-python">[0, 1, 2, 3]</code>'),
                           nom="List.append (2)",
                           texte="""
                           Quel est le contenu de la variable
                           <code class="prettyprint lang-python">l1</code>
                           après exécution de ces quatre lignes ?"""))

    e3 = exercice("Conversion héxadécimal <-> binaire",
                  "Cochez toutes les égalités vraies:",
                  question(réponse_vraie(r"$\small\tt{10001010010}_2 = \tt{452}_{16}$"),
                           réponse_vraie(r"$\small\tt{C9C}_{16} = \tt{110010011100}_2$"),
                           réponse_fausse(r"$\small\tt{EF4}_{16} = \tt{111011111000}_2$"),
                           réponse_fausse(r"$\small\tt{F66}_{16} = \tt{111101100111}_2$"),
                           réponse_fausse(r"$\small\tt{F2C}_{16} = \tt{111100101111}_2$"),
                           réponse_fausse(r"$\small\tt{10111100100}_2 = \tt{5E2}_{16}$"),
                           réponse_fausse(r"$\small\tt{111001111010}_2 = \tt{E75}_{16}$"),
                           réponse_fausse(r"$\small\tt{E8}_{16} = \tt{11101100}_2$"),
                           réponse_fausse(r"Aucune: elles sont toutes fausses !"),
                           colonnes=3))

    return json.dumps([e1, e2, e3], indent=4, separators=(',', ': '), ensure_ascii=False)


if __name__ == '__main__':
    print(toutes_les_questions())
