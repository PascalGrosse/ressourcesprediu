import csv

pokedex = []

with open("pokemons.csv") as f:
    for pokemon in csv.DictReader(f):
        pokedex.append(pokemon)

for pokemon in pokedex:
    print("Pokemon: {} génération: {}".format(pokemon["name"], pokemon["generation"]))

print("Il y a {} pokemons dans le pokedex".format(len(pokedex)))